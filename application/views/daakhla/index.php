<?php error_reporting(0)?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<!-- /* Team Daakha.pk @ Nest i/o */ -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="<?php echo base_url()?>assets/images/d.png" type="image/x-icon"/>
    <title>Daakhla.pk</title>
    <link href="<?php echo base_url()?>assets/tools/style.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="<?php echo base_url()?>assets/tools/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/tools/cufon-yui.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/tools/Abraham_Lincoln_400.font.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/tools/Inspiration_400.font.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>tools/Museo_Slab_100_400-Museo_Slab_700_400.font.js"></script>
    <script src="<?php echo base_url()?>assets/tools/jquery.validate.min.js"></script>
    <script src="<?php echo base_url()?>assets/tools/additional-methods.min.js"></script>
    <script type="text/javascript">
        Cufon.replace('.logo h1', {fontFamily: 'Inspiration'});
        Cufon.replace('.logo h2', {fontFamily: 'Museo Slab 100'});
        Cufon.replace('.logo h2 span', {fontFamily: 'Abraham Lincoln'});
        Cufon.replace('p.big_text, p.small_text', {fontFamily: 'Museo Slab 100'});
        Cufon.replace('p.big_text strong, p.small_text strong', {fontFamily: 'Museo Slab 700'});
    </script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>


</head>
<body>
<div id="transy">
</div>
<div id="wrapper">

    <div class="logo" align="center"><img  height="250"  src="<?php echo base_url()?>assets/images/Logo 3.png" /></div>
    <div class="content" style="top:-500px;">
        <p  class="big_text"><strong>we are working on something</strong> very interesting!</p>
        <p  class="small_text"><strong>be notified.</strong> we just need your email address.</p>
        <div class="form" style="min-height: 100px;">
            <div class="field_content">
                <form id="myForm" action="">
                    <input class="field" type="email" id="email" type="text"  />

                    <input class="submit" id="submit" type="button" value="" />
                </form>
            </div>
        </div>
        <div class="clear"></div>
        <p align="center" id="show_error" style="display:none; color: red;" ></p>
        <ul class="social">
            <li class="pinterest"><a href="#"></a></li>
            <li class="instagram"><a href="#"></a></li>
            <li class="twitter"><a href="#"></a></li>
            <li class="facebook"><a href="#"></a></li>
        </ul>


    </div>
    <p align="center" style="display:none"  class="big_text" id="show_msg"></p>
    <div style="height:400px; ">
        <a href="http://thenestio.com"><img height="50" src="<?php echo base_url()?>assets/images/nest.png" /></a><a href="https://www.googleforentrepreneurs.com/startup-communities/nest-i-o/"><img height="50" style="float:right"  src="<?php echo base_url()?>assets/images/g.png" /></a></div>

</div>
</body>

<script type="text/javascript">
    $(document).ready(function() {
        $("#submit").click(function () {
            var email = $('#email').val();
            var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
            if (!testEmail.test(email) || email == null){
                $("#show_error").html("*Invalid Email!").show(1000);
            }
            else {
                jQuery.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>" + "index.php/daakhla/submit_email",
                    dataType: "json",
                    data: {email: email},
                    success: function (res) {
                        if (res != false) {
                            $(".content").hide(1000);
                            $("#show_msg").html("Thank you! for subscribing").show();
                        }
                        else {

                            $(".content").hide(1000);
                            $("#show_msg").html("Your Email is already registered for our Newsletter!").show();
                        }


                    }
                });
            }
        });
    });

</script>

</html>
