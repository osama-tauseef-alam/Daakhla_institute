<?php

class Application_model extends CI_Model{
    public function __construct(){
        $this->load->database();
    }

    public function add_application($data){
        return $this->db-insert('application',$data);
    }

    public function view_application($applicant_id){
        $query = $this->db->get_where('application',array('applicant_id' => $applicant_id));
        return $query->result_array();
    }

    public function update_application($data,$applicant_id){
        return $this->db->update('application',$data,array('applicant_id'=>$applicant_id));
    }
}