<?php

/**
 * Created by PhpStorm.
 * User: Osama
 * Date: 8/4/2015
 * Time: 3:09 AM
 */
class Newsletter_model extends CI_Model
{
    public function __construct(){
        $this->load->database();
    }

    public function set_newsletter(){
        $this->load->helper('url');

        $data = array(
            'email' => $this->input->post('email')
        );

        return $this->db->insert('newsletter',$data);
    }
}