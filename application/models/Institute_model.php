<?php 
	class institute_model extends CI_Model{
		public function __construct(){
			$this->load->database();
		}

		public function insert_basic_info($data){
			return $this->db->insert('institute_basic_info',$data);
		}

		public function view_basic_info($institute_id){
			$query =  $this->db->get_where('institute_basic_info', array('institute_id' =>  $institute_id));
			return $query -> row_array();
		}

		public function update_basic_info($data,$institute_id){
			return $this->db->update('institute_basic_info',$data, array('institute_id' =>  $institute_id));
		}

		public function delete_basic_info($institute_id){
			return $this->db->delete('institute_basic_info',array('institute_id' =>  $institute_id));
		}

		//Institute Facilities
		public function view_facilities($institute_id){
			$query = $this->db->get_where('institute_facility',array('institute_id' =>  $institute_id));
			return $query -> row_array();
		}

		public function add_facilities($data){
			return $this->db->insert('institute_facility',$data);
		}

		public function update_facilites($data,$institute_id){
			return $this->db->update('institute_facility',$data,array('institute_id' => $institute_id ));
		}

		public function delete_facilitites($institute_id){
			return $this->db->delete(array('institute_id' => $institute_id ));
		}
		

		//Signup
		public function add_institute($data){
			return $this->db->insert('institute_signup',$data);
		}

		public function view_all_institute(){
			$query = $this->db->get('institute_signup');
			return $query->result_array();
		}
	}
?>
