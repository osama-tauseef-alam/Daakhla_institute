<?php
class User_model extends CI_Model
{
    public function __construct(){
        $this->load->database();
    }

    public function signup_form(){
//        $this->load->helper('url');

        $data = array(
            'username' => $this->input->post('username'),
            'email' => $this->input->post('email'),
            'password' => md5($this->input->post('password'))
        );
        $data = $this->security->xss_clean($data);
        return $this->db->insert('signup',$data);
    }
}