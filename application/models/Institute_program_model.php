<?php 
	class Institute_program_model extends CI_Model{
		public function __construct(){
			$this->load->database();
		}

		public function add_institute_program($data){
			return $this->db->insert('institute_program',$data);
		}

		public function view_institute_program($institute_id){
			$query = $this->db->get_where('institute_program',array('institute_id' =>  $institute_id));
			return $query->result_array();
		}

		public function delete_institute_program($institute_id,$program_id){
			return $this->db->delete('institute_program', array('institute_id' => $institute_id, 'program_id' => $program_id ));

		}

		public function update_institute_program($data,$institute_id,$program_id){
			return $this->db->update('institute_program',$data, array('institute_id' => $institute_id, 'program_id' => $program_id));
		}


	}
?>