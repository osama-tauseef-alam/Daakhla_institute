<?php
class Daakhla_model extends CI_Model
{
    public function __construct(){
        $this->load->database();
    }

    public function insert_email(){
        $this->load->helper("url");

        $data = array(
            'email' => $this->input->post('email')
        );

        return $this->db->insert('newsletter',$data);
    }

    public function get_email($email){
        $query = $this->db->get_where('newsletter',array('email'=> $email));
        return $query -> row_array();
    }
}