<?php 
	class Institute_Program extends CI_Controller{
		public function __construct(){
			parent::__construct();
			$this->load->model('institute_program_model');
		}

		public function add_institute_program(){
			// $data = array(
			// 	'institute_id' =>  $this->input->post('institute_id'),
			// 	'program_field' => $this->input->post('program_field'),
			// 	'admission_start' => $this->input->post('admission_start'),
			// 	'admission_end' => $this->input->post('admission_end'),
			// 	'admission_flag' => $this->input->post('admission_flag'),
			// 	'program_summary' => $this->input->post('program_summary'),
			// 	'program_criteria' => $this->input->post('program_criteria'),
			// 	'program_outline' => $this->input->post('program_outline'),
			// 	'program_duration' => $this->input->post('program_duration'),
			// 	'program_type' => $this->input->post('program_type')
			// 	);

			//************* Below is the sample test for add query *****************//

			$data = array(
				'institute_id' =>  '1',
				'program_field' => 'Economics And Finance',
				'admission_start' => '20/08/2015',
				'admission_end' => '30/08/2015',
				'admission_flag' => 'false',
				'program_summary' => 'fox jumps of the lazy dog. fox jumps of the lazy dog. fox jumps of the lazy dog. fox jumps of the lazy dog. ',
				'program_criteria' => 'minimum 45%',
				'program_outline' => 'bleh be bleh!',
				'program_duration' => '4 Years',
				'program_type' => 'MS'
				);


			$insert_data = $this->institute_program_model->add_institute_program($data);
			// print_r($insert_data);
			if($insert_data){
				$data['status'] = "Your Programs has been entered successfuly!";
			}
			else{
				$data['status'] = "Error! Kindly Contact the team of Daakhla";
			}
		}

		public function view_institute_program(){
			$institute_id = 1;
			$data['view_institute_program'] = $this->institute_program_model->view_institute_program($institute_id);

			$this->load->view('daakhla/testing',$data);
		}

		public function delete_institute_program(){
			$institute_id = 1;
			$program_id = 10;
			$delete_data = $this->institute_program_model->delete_institute_program($institute_id,$program_id);
			if($delete_data){
				$data['status'] = "Your Programs has been deleted successfuly!";
			}
			else{
				$data['status'] = "Error! Kindly Contact the team of Daakhla";
			}
			$this->load->view('daakhla/testing',$data);
		}

		public function update_institute_program(){
			// $data = array(
			// 	'institute_id' =>  $this->input->post('institute_id'),
			// 	'program_field' => $this->input->post('program_field'),
			// 	'admission_start' => $this->input->post('admission_start'),
			// 	'admission_end' => $this->input->post('admission_end'),
			// 	'admission_flag' => $this->input->post('admission_flag'),
			// 	'program_summary' => $this->input->post('program_summary'),
			// 	'program_criteria' => $this->input->post('program_criteria'),
			// 	'program_outline' => $this->input->post('program_outline'),
			// 	'program_duration' => $this->input->post('program_duration'),
			// 	'program_type' => $this->input->post('program_type')
			// 	);

			//************* Below is the sample test for add query *****************//

			$data = array(
				'institute_id' =>  '1',
				'program_field' => 'Update Economic',
				'admission_start' => '20/08/2015',
				'admission_end' => '30/08/2015',
				'admission_flag' => 'false',
				'program_summary' => 'fox jumps of the lazy dog. fox jumps of the lazy dog. fox jumps of the lazy dog. fox jumps of the lazy dog. ',
				'program_criteria' => 'minimum 45%',
				'program_outline' => 'bleh be bleh!',
				'program_duration' => '4 Years',
				'program_type' => 'MS'
				);

			$program_id = 9;
			$institute_id = 1;

				$update_data = $this->institute_program_model->update_institute_program($data,$institute_id,$program_id);
				if($update_data){
					$data['status'] = "Your Programs has been update successfuly!";
				}
				else{
					$data['status'] = "Error! Kindly Contact the team of Daakhla";
				}
		}


	}
?>