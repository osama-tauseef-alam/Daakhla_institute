<?php
	class Applicant extends CI_Controller{
		public function __construct(){
			parent::__construct();
			$this->load->model('applicant_model');
		}

		public function update_applicant_info(){
			$data = array(
				'applicant_email' => $this->input->post('applicant_email'), 
				'applicant_name' => $this->input->post('applicant_name'),
				'father_name' => $this->input->post('father_name'),
				'contact_phone' => $this->input->post('contact_phone'),
				'contact_mobile' => $this->input->post('contact_mobile'),
				'applicant_cnic' => $this->input->post('applicant_cnic'),
				'applicant_gender' => $this->input->post('applicant_gender'),
				'applicant_address' => $this->input->post('applicant_address'),
				'applicant_city' => $this->input->post('applicant_city'),
				'applicant_country' => $this->input->post('applicant_country'),
				'applicant_nationality' => $this->input->post('applicant_nationality'),
				'applicant_dob' => $this->input->post('applicant_dob'),
				'applicant_interest' => $this->input->post('applcant_interest'),
				'applicant_password' => $this->input->post('applicant_password')
				);
			$update_data = $this->applicant_model->update_applicant_info($data);
			if($update_data){
				$data['status'] = 'Your Information has been successfuly updated1';
			}
			else{
				$data['status'] = 'Kindly Contact Team Daakhla';
			}
			$this->load->view('daakhla/testing',$data);
		}
	}
?>