<?php
	class applications extends CI_Controller{
		public function __construct(){
			parent::__construct();
			$this->load->model('application_model');
		}

		public function add_application(){
			$data = array(
				'applicant_id' => $this->input->post('applicant_id'),  
				'institute_id' => $this->input->post('institute_id'),
				'program_id' => $this->input->post('program_id'),
				'application_date' => date('Y-m-d'),
				'application_status' => 'Pending'
				);

			$insert_data = $this->application_model->add_application($data);
			if($insert_data){
				$status = 'Your Application has been submited!';
			}
			else{
				$status = 'Kindly contact the team Daakhla!';
			}
			$this->load->view('daakhla/testing',$data);
		}

		public function view_application(){
            $application_id = 1;
            $data['view_application'] = $this->application_model->view_application($application_id);
            $this->load->view('daakhla/testing',$data);
		}

        public function update_application(){
            $data =  array(
                'application_status' => $this->input->post('application_status'),
                'application_feedback' => $this->input->post('application_feedback')
            );

            $application_id = 1;
            $update_data = $this->application_model->update_application($data,$application_id);
            if($update_data){
                $data['status'] = 'Your Application has been submited!';
            }
            else{
                $data['status'] = 'Kindly contact the team Daakhla!';
            }
            $this->load->view('daakhla/testing',$data);
        }
	}
?>