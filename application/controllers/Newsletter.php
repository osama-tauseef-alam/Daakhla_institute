<?php

class Newsletter extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->model('newsletter_model');
    }

    public function index(){
        $this->load->helper('form');
        $this->load->library('form_validation');
//
        $data['title'] = 'Subscribe Now!';
//
//        $this->form_validation->set_rules('email','Email','required');
//
//        if($this->form_validation->run() === FALSE){
            $this->load->view('newsletter/index',$data);

//        }
//        else{
//            $this->newsletter_model->set_newsletter();
//            echo json_encode($data);
//        }
    }

    public function submit_email(){
        $data = array(
            'email' => $this->input->post('email')
        );
        echo json_encode($data);
    }
}