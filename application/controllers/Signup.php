<?php

class Signup extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->model('user_model');
    }

    public function index(){
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->view('daakhla/signup');
    }

    public function signup(){

        $this->load->library('form_validation');
        $this->form_validation->set_rules('username','Text','required');
        $this->form_validation->set_rules('password','Password','required');
        $this->form_validation->set_rules('email','Email','required');

        if($this->form_validation->run() === false){
            $this->load->view('daakhla/signup');
        }
        else {
            $this->user_model->signup_form();
            $this->load->view('daakhla/success');
        }
    }
}