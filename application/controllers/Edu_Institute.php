<?php
class edu_institute extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('institute_model');
    }
    //Basic info of institute
    public function add_basic_info(){
        $data = array(
            'institute_name' => $this->input->post('institute_name'),
            'institute_cover_img' => $this->input->post('institute_cover_image'),
            'institute_email' => $this->input->post('institute_email'),
            'institute_short_intro' => $this->input->post('institute_short_intro'),
            'institute_contact' => $this->input->post('institute_contact'),
            'institute_address' => $this->input->post('institute_address'),
            'institute_logo' => $this->input->post('institute_logo'),
            'institute_summary' => $this->input->post('institute_summary'),
            'institute_location' => $this->input->post('institute_location'),
        );

        $insert_data = $this->institute_model->add_basic_info($data);
        if($insert_data){
            $status = "Your Basic info has been inserted";
        }
        else{
            $status = "Error! Kindly Contact the team of Daakhla";
        }

        $this->load->view('daakhla/basic_profile',$status);
    }

    public function update_basic_info(){
        $data = array(
            'institute_name' => $this->input->post('institute_name'),
            'institute_cover_img' => $this->input->post('institute_cover_image'),
            'institute_email' => $this->input->post('institute_email'),
            'institute_short_intro' => $this->input->post('institute_short_intro'),
            'institute_contact' => $this->input->post('institute_contact'),
            'institute_address' => $this->input->post('institute_address'),
            'institute_logo' => $this->input->post('institute_logo'),
            'institute_summary' => $this->input->post('institute_summary'),
            'institute_location' => $this->input->post('institute_location'),
        );
        $institute_id = 1;
        $update_data = $this->institute_model->update_basic_info($data,$institute_id);
        if($update_data){
            $status = "Your Basic info has been Updated";
        }
        else{
            $status = "Error! Kindly Contact the team of Daakhla";
        }

        $this->load->view('daakhla/testing',$status);
    }

    public function view_basic_info(){
        //after development complete we change institute_id to session id for institute
        $institute_id = 1;
        $data['institute_basic_info'] = $this->institute_model->view_basic_info($institute_id);

        $this->load->view('daakhla/view_basic_info',$data);
    }

    public function delete_basic_info(){
        $institute_id = 1;
        $delete_data = $this->institute_model->delete_basic_info($institute_id);
        if($delete_data){
            $status = "Your Basic info has been Deleted";
        }
        else{
            $status = "Error! Kindly Contact the team of Daakhla";
        }
    }





    //Facilitites
    public function add_facilities(){
        $data = array(
            'institute_id' => $this->input->post('institute_id'),
            'facility_name' => $this->input->post('facility_name')
        );
        $insert_data = $this->institute_model->add_facilities($data);
        if($insert_data){
            $status = "Your Program Faclities has been inserted";
        }
        else{
            $status = "Error! Kindly Contact the team of Daakhla";
        }
    }

    public function view_facilities(){
        $institute_id = 1;
        $data['institute_facility'] = $this->institute_model->view_facilities($institute_id);
        $this->load->view('daakhla/testing',$data);
    }

    public function update_facilities(){
        $data = array(
            'institute_id' => $this->input->post('institute_id'),
            'facility_name' => $this->input->post('facility_name')
        );
        $institute_id = 1;
        $update_data = $this->institute_model->update_facilities($data,$institute_id);
        if($update_data){
            $status = "Your Program Facilities has been Updated";
        }
        else{
            $status = "Error! Kindly Contact the team of Daakhla";
        }
    }

    public function delete_facilities(){
        $institute_id = 1;
        $delete_data = $this->institute_model->delete_facilities($institute_id);
        if($delete_data){
            $status = "Your Facilities has been Deleted";
        }
        else{
            $status = "Error! Kindly Contact the team of Daakhla";
        }
    }

    public function signup(){
        $data = array(
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
        );

        $insert_data = $this->institute_model->add_institute($data);
        if($insert_data){
            $status = "Your Educational Insitute has been soon listed our assistant will contact on your given email!";
        }
        else{
            $status = "Error! Kindly Contact the team of Daakhla";
        }
    }

    public function view_all_signup_institute(){
        $data['all_institute'] = $this->institute_model->view_all_institute();
        $this->load->view('daakhla/testing',$data);
    }

    public function signup_mail($to){
        $this->load->library('email');
        $this->email->from('info@daakhla.pk','Daakhla.pk Team');
        $this->email->to($to);
        $this->email->subject('Educational Institute Registration Request at Daakhla.pk');

        $message = "
            <html>
            <head>
            <title>Daakhla.pk</title>
            </head>
            <body>
            <div>
                <img src='../../assets/images/Logo%203.png';
            </div>
            <p>This email contains HTML Tags!</p>
            <table>
            <tr>
            <th>Firstname</th>
            <th>Lastname</th>
            </tr>
            <tr>
            <td>John</td>
            <td>Doe</td>
            </tr>
            </table>
            </body>
            </html>
        ";
    }

}
?>
