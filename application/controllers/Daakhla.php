<?php
class Daakhla extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->model('daakhla_model');
    }

    public function index(){
        $this->load->helper('url');
        $this->load->view('daakhla/index');
    }

    public function submit_email(){
        $email = $this->input->post('email');
        $already_exist =$this->daakhla_model->get_email($email);
        if($already_exist){
            $data = False;
        }
        else {
            $data = array(
                'email' => $this->input->post('email')
            );
            $this->daakhla_model->insert_email();
        }
        echo json_encode($data);
    }

}